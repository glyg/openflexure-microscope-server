Settings pane
=============

The settings for the microscope are gathered together into a settings pane, which is further subdivided into sections.  This page does not provide an exhaustive list, but a few of the notable controls are:

* Adjusting exposure time and gain of the camera, including automatic adjustment.
* Automatic white balance and flat-field correction for the camera.
* Enabling or disabling certain features of the software (e.g. ImJoy integration).
* Enabling or disabling the video stream (this allows the native low-latency preview on the Raspberry Pi to be used instead).
* Calibrating the relationship between stage coordinates and pixel coordinates in the video stream, allowing click-to-move to function.

Important calibration tasks (in particular camera settings adjustment and click-to-move calibration) will be prompted in a "wizard" dialogue when the software is first run, to help first time users set up their microscope.  All of the auto calibration routines are also available from the settings pane.

.. image:: pane_settings.png