Microscope class
=======================================================

The main microscope class handles microscope settings, passing these between the settings file and their appropriate components (camera, stage), basic metadata about the current device status, and interfacing with the separate camera and stage components.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

.. automodule:: openflexure_microscope.microscope
   :members: