Welcome to OpenFlexure Microscope Software's documentation!
===========================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   quickstart.rst
   webapp/index.rst
   config.rst
   microscope.rst
   camera.rst
   stage.rst
   plugins.rst
   api.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
