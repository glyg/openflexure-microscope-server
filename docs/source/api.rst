HTTP API
========

Live documentation
------------------

Full, interactive Swagger documentation for your microscopes web API is available from the microscope itself. From any browser, go to ``http://{your microscope IP address}/api/v2/docs/swagger-ui``.

The API is described in an OpenAPI description, available at ``http://{your microscope IP address}/api/v2/docs/openapi.yaml``.  It is also available from our `build server`_.  It can be conveniently viewed using `Redoc's online preview`_.

.. _`build server`: https://build.openflexure.org/openflexure-microscope-server/
.. _`Redoc's online preview`: https://redocly.github.io/redoc/?url=https://build.openflexure.org/openflexure-microscope-server/latest-api.yaml